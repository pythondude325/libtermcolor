/*
    MIT License
    Copyright (c) 2018 Graham Scheaffer

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include <stdint.h>

// Hue macros
#define HUE_RED (0.0 / 6.0)
#define HUE_YELLOW (1.0 / 6.0)
#define HUE_GREEN (2.0 / 6.0)
#define HUE_CYAN (3.0 / 6.0)
#define HUE_BLUE (4.0 / 6.0)
#define HUE_MAGENTA (5.0 / 6.0)

double clamp_double(double n);
uint8_t double_to_byte(double n);

typedef struct {
    double r;
    double g;
    double b;
} Color;

void set_rgb_color(Color *c, double r, double g, double b);

double calc_hue(double h);
double calc_sv(double i, double s, double v);
void set_hsv_color(Color *c, double h, double s, double v);

void print_bgcolor(const char *str, const Color *c);
void print_fgcolor(const char *str, const Color *c);
