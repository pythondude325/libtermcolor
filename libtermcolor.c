/*
    MIT License
    Copyright (c) 2018 Graham Scheaffer

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "libtermcolor.h"

#define ONE_THIRD ((1.0)/(3.0))

double clamp_double(double n){
    n = (n > 1.0) ? 1.0 : n;
    n = (n < 0.0) ? 0.0 : n;
    return n;
}

uint8_t double_to_byte(double n){
    n = n * 255;
    n = (n > 255) ? 255 : n;
    n = (n < 0) ? 0 : n;
    return (uint8_t)n;
}

void set_rgb_color(Color *c, double r, double g, double b){
    c->r = r;
    c->g = g;
    c->b = b;
}

double calc_hue(double h){
    return clamp_double(-6 * fabs(ONE_THIRD - fmod(h,1)) + 2);
}

double calc_sv(double i, double s, double v){
    return s * v * (i - 1.0) + v;
}

void set_hsv_color(Color *c, double h, double s, double v){
    double r, g, b;
    r = calc_sv(calc_hue(h + ONE_THIRD), s, v);
    g = calc_sv(calc_hue(h), s, v);
    b = calc_sv(calc_hue(h - ONE_THIRD), s, v);
    set_rgb_color(c, r, g, b);
}

void print_hex_codes(const Color *c, char seperator){
    printf("%c%d", seperator, double_to_byte(c->r));
    printf("%c%d", seperator, double_to_byte(c->g));
    printf("%c%d", seperator, double_to_byte(c->b));
}

void print_bgcolor(const char *str, const Color *c){
    printf("\e[48;2");
    print_hex_codes(c, ';');
    printf("m%s\e[0m", str);
}

void print_fgcolor(const char *str, const Color *c){
    printf("\e[38;2");
    print_hex_codes(c, ';');
    printf("m%s\e[0m", str);
}

